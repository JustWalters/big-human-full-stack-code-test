'use strict';

const { User } = require('./models/user');

async function users(req, res, next) {
	let projection = 'name email';
	try {
		let users = await User.find({}, projection);
		return res.json(users);
	} catch (err) {
		return next(err);
	}
}

module.exports = {
		users
};
