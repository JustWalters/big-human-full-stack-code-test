## Dependencies
- Node.js and npm (https://nodejs.org)
- MongoDB (https://docs.mongodb.com/manual/installation/)
- nodemon (optional) (npm i -g nodemon)

## Running
After confirming you have the above dependcies, keep mongodb running, run `npm install` to install the necessary node modules.

Styles are written using [Less](http://lesscss.org), and must be compiled to css before being included. To do this, run `npm run build`. Note: Currently, this will only build a single less file named `main.less`. After building, the server can be started by running `node index.js`, with two environment variables supported.

Set `PORT` to change the port that the server listens on. The default is 3000. You may also set `CREATE_ADMIN_USER` to true. This will create and save a user with heightened privileges and print the email and password to the console.

## Testing
To test, run `npm test`.

## Design Decisions
Front-end libraries Vue.js and axios are included as external scripts purely to ease development. Vue.js supports most major browsers excluding Internet Explorer 8 and below, so I aimed to support the same. Because Internet Explorer still does not support the modern fetch api, I would either need to use the lower-level XMLHttpRequest (which is not available in Opera Mini), include a polyfill, or a library like axios. The additional cost of the library is worth it for the friendlier api.

I chose to use Less because it has fewer dependencies than Sass, which requires Ruby to be installed, and I did not require any features that Less does not support.
