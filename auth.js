'use strict';

const { User } = require('./models/user');

async function signup(req, res, next) {
	let { name, email, password } = req.body;
	if (!name || !email || !password) {
		let err = new Error('Name, email, and password are required');
		err.status = 422;
		return next(err);
	}

	let user = new User({
		name,
		email,
		password
	});

	try {
		let newUser = await	user.save();
		res.json(newUser);
	} catch (e) {
		return next(err);
	}
}

async function login(req, res, next) {
	let { email, password } = req.body;
	if (!email || !password) {
		let err = new Error('Email and password are required');
		err.status = 422;
		return next(err);
	}

	try {
		let user = await User.authenticate(email, password);
		if (user) {
			req.session.user = user;
			return res.json(user);
		} else {
			let err = new Error('Invalid user/password combination');
			err.status = 401;
			return next(err);
		}
	} catch(e) {
		return next(e);
	}
}

function logout(req, res, next) {
	req.session.destroy(function(err) {
		if (err) return next(err);
		res.json({});
	});
}

function me(req, res, next) {
	let user = req.session.user;
	if (user) return res.json(user);

	let err = new Error('Please log in first');
	err.status = 401;
	return next(err);
}

function isAdmin(req, res, next) {
	let user = req.session.user;
	if (user && user.isAdmin) {
		return next();
	}

	let err = new Error('You do not have access to this');
	err.status = 401;
	return next(err);
}

module.exports = {
	signup,
	login,
	logout,
	me,

	// Middleware functions
	middle: {
		isAdmin
	}
};
