'use strict';

const mongoose = require('mongoose');
const { app } = require('./server');

const PORT = process.env.PORT || 3000;
mongoose.Promise = global.Promise;

let mongoUri = 'mongodb://localhost/big-human-demo';
let mongoOpts = { useMongoClient: true };
mongoose.connect(mongoUri, mongoOpts)
	.then(init)
	.catch(connectionError);

function init() {
	if (process.env.CREATE_ADMIN_USER) require('./scripts/createAdmin');
	app.listen(PORT, function() {
		console.log(`Server listening on port ${PORT}`);
	});
}

function connectionError(err) {
	console.error(err);
	process.exit(1);
}
