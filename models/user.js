'use strict';

const mongoose = require('mongoose');
const validators = require('mongoose-validators');
const bcrypt = require('bcrypt');

const errMessages = {
	invalidEmail: 'The email you entered is invalid',
	emailTaken: 'The specified email address is already in use',
	passwordLength: 'Password must contain between 6 and 72 characters'
};

const UserSchema = mongoose.Schema({
	name: {
		type: String,
		trim: true,
		required: true
	},
	email: {
		type: String,
		trim: true,
		required: true,
		validate: validators.isEmail({
			message: errMessages.invalidEmail
		}),
	},
	password: {
		type: String,
		required: true,
		// Min length chosen arbitrarily. Max length comes from bcrpyt docs, which say:
		// "Per bcrypt implementation, only the first 72 characters of a string are used"
		validate: validators.isLength({ message: errMessages.passwordLength }, 6, 72)
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});

UserSchema
	.path('email')
	.validate(async function(value) {
		let user = await this.constructor.findOne({ email: value });
		if (!user || this.id === user.id) {
			return true;
		}

		return false;
	}, errMessages.emailTaken);

UserSchema
	.pre('save', function(next) {
		// Right now, user only saved on creation so we know password isn't already hashed
		let user = this;
		let saltRounds = 10; // bcrypt docs use 10 for this parameter
		bcrypt.hash(user.password, saltRounds, function(err, hash) {
			if (err) return next(err);

			user.password = hash;
			return next();
		});
	});

// Returns the user or false
UserSchema.statics.authenticate = async function(email, password) {
	let user = await User.findOne({ email });
	if (!user) return false;

	let match = await bcrypt.compare(password, user.password);
	if (!match) return false;

	user.password = undefined;
	return user;
};

const User = mongoose.model('User', UserSchema);

module.exports = {
	User
};
