'use strict';

const path = require('path');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const auth = require('./auth');
const api = require('./api');

const app = express();
app.use(bodyParser.json());
app.use(session({
	secret: 'bighuman',
	resave: false,
	saveUninitialized: false
}));

app.post('/signup', auth.signup);
app.post('/login', auth.login);
app.post('/logout', auth.logout);
app.get('/me', auth.me);

app.get('/users', auth.middle.isAdmin, api.users);

app.use('/', express.static(path.join(__dirname, '/public')));

app.use(function(err, req, res, next) {
	let status = err.status || 500;
	res.status(status);
	res.json(err.message);
});

module.exports = {
	app
};
