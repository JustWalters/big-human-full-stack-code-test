'use strict';

const { User } = require('../models/user');

let name = 'Admin User';
let email = `admin${getRandomNum()}@gmail.com`;
let password = generatePassword();

let user = new User({
	name,
	email,
	password,
	isAdmin: true
});

user.save()
	.then(function(savedUser) {
		// Use saved email because in the future it could be transformed on save (e.g. lowercased,
		// stripped of anything after a + in the local part). Use original password because
		// hashed version is saved and returned here
		let result = `
		You can now login to your admin account using these credentials. Please keep them safe
		Email: ${savedUser.email}
		Password: ${password}`;
		console.log(result);
	})
	.catch(function(err) {
		let result = `Something went wrong. This is what we know: ${err.message} Please try again.`;
		console.log(result);
	});

function getRandomNum(max = 100) {
	return Math.floor(Math.random() * max);
}

function generatePassword() {
	let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	let charsLen = chars.length;
	let passLength = Math.max(7, getRandomNum(72));
	let res = '';
	for (let i = 0; i < passLength; i++) {
		res += chars[getRandomNum(charsLen)];
	}
	return res;
}
