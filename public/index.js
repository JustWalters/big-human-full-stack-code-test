'use strict';

let data = {
	loggedIn: false,
	loggingIn: false,

	name: '',
	email: '',
	password: '',
	isAdmin: false,

	errorMessage: ''
};

let methods = {
	signup(data) {
		axios.post('/signup', data)
			.then(this.updateData)
			.then(() => this.loggedIn = true)
			.catch(this.handleError);
	},
	login(data) {
		axios.post('/login', data)
			.then(this.updateData)
			.then(() => this.loggedIn = true)
			.catch(this.handleError);
	},
	logout() {
		axios.post('/logout')
			.then(this.updateData)
			.then(() => this.loggedIn = false)
			.catch(this.handleError);
	},
	signUpOrIn(data) {
		if (this.loggingIn) this.login(data);
		else this.signup(data);
	},
	updateData(response) {
		let data = response.data;
		let keys = ['name', 'email', 'isAdmin'];
		keys.forEach(key => this[key] = data[key]);
		return response;
	},

	toggleLoggedOutForm() {
		this.loggingIn = !this.loggingIn;
	},

	handleError(err) {
		let msg = err.response && err.response.data || 'Sorry, there was an error';
		this.errorMessage = msg;
	},
	closeError() {
		this.errorMessage = '';
	}
};

let computed = {
	loggedOutButtonText() {
		return this.loggingIn ? 'I want to sign up' : 'I want to log in';
	},
	headerText() {
		if (this.loggedIn) return '';
		return this.loggingIn ? 'Sign In' : 'Sign Up';
	},
	dataProps() {
		let inputs = [{
			name: 'email',
			type: 'email'
		}, {
			name: 'password',
			type: 'password'
		}];

		if (!this.loggingIn) {
			inputs.unshift({
				name: 'name',
				type: 'text'
			});
		}

		return {
			inputs
		};
	}
};

new Vue({
	el: "#app",
	data,
	computed,
	methods,
	created() {
		axios.get('/me')
			.then(res => {
				// If we get a user id (i.e. are logged in), update the view accordingly
				if (res && res.data && res.data._id) {
					this.loggedIn = true;
					return res;
				}
				return Promise.reject(res);
			})
			.then(this.updateData)
			// We don't care about this rejection but need to acknowledge it
			.catch(() => {});
	}
});
