'use strict';

// Assumes all input elements are very simply named, and all inputs are required.
let props = {
	inputs: {
		type: Array,
		required: true
	}
};

let template = `
<form v-on:submit.prevent="handleSubmit">
	<template v-for="inp in inputs">
		<input
			required
			class="simple-form-input"
			v-model="$data[inp.name]"
			v-bind:placeholder="inp.name | capitalize"
			v-bind:type="inp.type"
			v-bind:name="inp.name">
		<br>
	</template>
	<input type="submit" class="btn-simple-form-submit" value="Submit">
</form>`;

let simpleFormOptions = {
	filters: {
		capitalize(value) {
			if (!value) return '';
			value = value.toString();
			return value.charAt(0).toUpperCase() + value.slice(1);
		}
	},
	props,
	data() {
		let res = {};
		this.inputs.forEach(input => res[input.name] = '');
		return res;
	},
	methods: {
		handleSubmit() {
			this.$emit('simple-submit', this.$data);
		}
	},
	template
};

Vue.component('simple-form', simpleFormOptions);
