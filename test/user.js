'use strict';

const assert = require('assert');
const mongoose = require('mongoose');
const { User } = require('../models/user');

describe('User Model', function() {
	before(function(done) {
		mongoose.connect('mongodb://localhost/big-human-test', {
			useMongoClient: true
		}, done);
	});

	after(function() {
		mongoose.connection.close();
	});

	describe('validation', function() {
		after(function(done) {
			User.remove({}, done);
		});

		describe('email', function() {
			let user, email;

			beforeEach(function() {
				email = 'bob@gmail.com';
				user = new User({
					name: 'name',
					password: 'password'
				});
			});

			it('should reject invalid emails', function(done) {
				user.email = 'noatsignemail.com';
				user.save()
					.then(done)
					.catch(function(err) {
						assert.equal(err.name, 'ValidationError');
						return done();
					});
			});

			it('should accept valid emails', function(done) {
				user.email = email;
				user.save()
					.then(function(savedUser) {
						assert.equal(savedUser.email, user.email);
						return done();
					})
					.catch(done);
			});

			it('should be unique', function(done) {
				user.email = email;
				user.save()
					.then(done)
					.catch(function(err) {
						assert.equal(err.name, 'ValidationError');
						return done();
					});
			});

		});

		describe('password', function() {
			let user;

			beforeEach(function() {
				user = new User({
					name: 'name',
					email: 'bob@email.com'
				});
			});

			it('should reject too short passwords', function(done) {
				user.password = 'pass';
				user.save()
					.then(done)
					.catch(function(err) {
						assert.equal(err.name, 'ValidationError');
						return done();
					});
			});

			it('should reject too long passwords', function(done) {
				let maxLength = 72;
				user.password = 'pass'.padEnd(maxLength + 1, 'pass');
				user.save()
					.then(done)
					.catch(function(err) {
						assert.equal(err.name, 'ValidationError');
						return done();
					});
			});

			it('should accept valid passwords', function(done) {
				user.password = 'goodpass';
				user.save()
					.then(function(savedUser) {
						return done();
					})
					.catch(done);
			});
		});

		it('should hash the password', function(done) {
			let name = 'name';
			let email = 'email@email.com';
			let password = 'password';

			let user = new User({
				name,
				email,
				password
			});

			user.save()
				.then(function(savedUser) {
					assert.equal(savedUser.name, name);
					assert.equal(savedUser.email, email);
					assert.notEqual(savedUser.password, password);
					return done();
				})
				.catch(done);
		});
	});

	describe('on save', function() {
		after(function(done) {
			User.remove({}, done);
		});

		it('should hash the password', function(done) {
			let name = 'name';
			let email = 'email@email.com';
			let password = 'password';

			let user = new User({
				name,
				email,
				password
			});

			user.save()
				.then(function(savedUser) {
					assert.equal(savedUser.name, name);
					assert.equal(savedUser.email, email);
					assert.notEqual(savedUser.password, password);
					return done();
				})
				.catch(done);
		});
	});

	describe('.authenticate', function() {
		let name, email, password;

		before(function(done) {
			name = 'name';
			email = 'email@email.com';
			password = 'password';

			let user = new User({
				name,
				email,
				password
			});

			user.save()
				.then(() => done())
				.catch(done);
		});

		after(function(done) {
			User.remove({}, done);
		});

		it('should return the user when called with correct email & password', async function() {
			let user = await User.authenticate(email, password);
			assert.equal(user.name, name);
			assert.equal(user.email, email);
			assert.ok(!user.password);
		});

		it('should return false when called with incorrect email', async function() {
			let user = await User.authenticate(email + 'extra', password);
			assert.ok(!user);
		});

		it('should return false when called with incorrect password', async function() {
			let user = await User.authenticate(email, password + 'extra');
			assert.ok(!user);
		});
	});
});
